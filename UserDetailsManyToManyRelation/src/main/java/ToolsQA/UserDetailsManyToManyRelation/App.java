package ToolsQA.UserDetailsManyToManyRelation;

import org.hibernate.Session;
import org.hibernate.Transaction;

import ToolsQA.UserDetailsManyToManyRelation.HibernateUtil;
import ToolsQA.UserDetailsManyToManyRelation.UserDetails;
import ToolsQA.UserDetailsManyToManyRelation.Vehicle;

public class App 
{
    public static void main( String[] args )
    { 
    	
    	UserDetails user = new UserDetails();
    	user.setUserName("John");
    	Vehicle vehicle = new Vehicle();
    	vehicle.setVehicleName("car");
    	Vehicle vehicle2 = new Vehicle();
    	vehicle2.setVehicleName("Jeep");
    	
    	user.getVehicle().add(vehicle);
    	user.getVehicle().add(vehicle2);
    	vehicle.getUserList().add(user);
    	vehicle2.getUserList().add(user);
    	
    	UserDetails user1 = new UserDetails();
    	user1.setUserName("Daniel"); 
    	UserDetails user2 = new UserDetails();
    	user2.setUserName("Jackson");
    	Vehicle vehicle1 = new Vehicle();
    	vehicle1.setVehicleName("Activa");
    	
    	user1.getVehicle().add(vehicle1);
    	user2.getVehicle().add(vehicle1);
    	vehicle1.getUserList().add(user1);
    	vehicle1.getUserList().add(user2);
         
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
        Transaction transaction = session.beginTransaction();
       
        session.save(user);
        session.save(vehicle);
        session.save(vehicle2);
        
        transaction.commit();
        
        transaction = session.beginTransaction();
        
        session.save(vehicle1);
        session.save(user1);
        session.save(user2);
        
        transaction.commit();

      } catch (Exception e) {
    	  System.out.println(e);
          
    }
    }
}
